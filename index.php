<?php
get_header(); 
?>

<?php if ( get_header_image() ) : ?>
	<div class="custom-header">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
            <img src="<?php header_image(); ?>" width="<?php echo absint( get_custom_header()->width ); ?>" height="<?php echo absint( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
        </a>
    </div>
<?php endif; ?>
       
        <section class="blog-section">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                    
          <?php
			/* CUSTOM POST PARCHI LOOP */
			$query = new WP_Query( array( 'post_type' => 'Parchi' ) );

			if( $query->have_posts() ) :
			$count = 1;
			 /* Start the Loop */
			 while ( $query->have_posts() ) : $query->the_post();
			 get_template_part( 'template-parts/content' );
			if ($count % 3 == 0) {
				echo "<div class='clearfix'></div>";
			}
			$count++;			 
			 endwhile;
			 
			echo "<div class='clearfix'></div>";			 
			 
			$post_args =  array(
				'screen_reader_text' => ' ',
				'prev_text' => __( '<div class="chevronne"><i class="fa fa-chevron-left"></i></div>', 'marinate' ),
				'next_text' => __( '<div class="chevronne"><i class="fa fa-chevron-right"></i></div>', 'marinate' ),
				);
	
				the_posts_pagination( $post_args );        
			 
			else :
			get_template_part( 'template-parts/content', 'none' );
			endif; 
		
		?>
                        
                        
                    </div>
                </div>
            </div>
        </section>
        
<?php  get_footer(); ?>