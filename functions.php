<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style' );
function enqueue_parent_theme_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

function cptui_register_my_cpts() {

	/**
	 * Post Type: Parchi.
	 */

	$labels = array(
		"name" => __( "Parchi", "" ),
		"singular_name" => __( "Parco", "" ),
		"all_items" => __( "Tutti i parchi", "" ),
		"add_new" => __( "Aggiungi Parco", "" ),
		"edit_item" => __( "Modifica Parco", "" ),
	);

	$args = array(
		"label" => __( "Parchi", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "parchi", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "post-formats" ),
		"taxonomies" => array( "post_tag", "tipo_parco" ),
	);

	register_post_type( "parchi", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );


function cptui_register_my_taxes_tipo_parco() {

	/**
	 * Taxonomy: Tipi Parchi.
	 */

	$labels = array(
		"name" => __( "Tipi Parchi", "" ),
		"singular_name" => __( "Tipo Parco", "" ),
		"menu_name" => __( "Tipologia parco", "" ),
		"all_items" => __( "Tutti i tipi", "" ),
		"edit_item" => __( "Modifica tipo", "" ),
		"view_item" => __( "Vedi tipo", "" ),
		"update_item" => __( "Aggiorna tipo", "" ),
		"add_new_item" => __( "Aggiungi tipo", "" ),
		"new_item_name" => __( "Nuovo nome per il tipo di parco", "" ),
		"search_items" => __( "Cerca tipo", "" ),
		"add_or_remove_items" => __( "Aggiungi o rimuovi tipo", "" ),
		"not_found" => __( "Nessun tipo trovato", "" ),
	);

	$args = array(
		"label" => __( "Tipi Parchi", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Tipi Parchi",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'tipo_parco', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "tipo_parco", array( "parchi" ), $args );
}

add_action( 'init', 'cptui_register_my_taxes_tipo_parco' );



 


/**  meta box per custom post parchi */

add_action( 'cmb2_admin_init', 'cmb2_parchi_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_parchi_metaboxes() {
	
	$prefix = '_ifts_informazioni';

	/**
	 * meta box informazioni
	 */
	$cmb_informazioni = new_cmb2_box( array(
		'id'            => 'ifts_informazioni_parco',
		'title'         => __( 'informazioni parco ', 'cmb2' ),
		'object_types'  => array( 'parchi', ), // Post type
		'context'       => 'advanced',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	$cmb_informazioni->add_field( array(
		'name'       => esc_html__( 'Indirizzo parco ', 'cmb2' ),
		'desc'       => esc_html__( 'Inserire indirizzo del parco ', 'cmb2' ),
		'id'         => $prefix . 'text',
		'type'       => 'text',
		'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
		// 'column'          => true, // Display field value in the admin post-listing columns
	) );
	$cmb_informazioni->add_field( array(
		'name' => esc_html__( 'Sito web parco', 'cmb2' ),
		'desc' => esc_html__( 'inserire sito web parco', 'cmb2' ),
		'id'   => $prefix . 'url',
		'type' => 'text_url',
		// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
		// 'repeatable' => true,
	) );
	$cmb_informazioni->add_field( array(
		'name' => esc_html__( 'Data apertura parco ', 'cmb2' ),
		'desc' => esc_html__( 'Inserire data di apertura parco ', 'cmb2' ),
		'id'   => $prefix . 'textdate_apertura',
		'type' => 'text_date',
		// 'date_format' => 'Y-m-d',
	) );
	$cmb_informazioni->add_field( array(
		'name' => esc_html__( 'Data chiusura parco', 'cmb2' ),
		'desc' => esc_html__( 'Inserire data di chiusura parco ', 'cmb2' ),
		'id'   => $prefix . 'textdate_chiusura',
		'type' => 'text_date',
		// 'date_format' => 'Y-m-d',
	) );
	$cmb_informazioni->add_field( array(
		'name' => esc_html__( 'Checkbox disabili', 'cmb2' ),
		'desc' => esc_html__( 'Accessibilità per disabili', 'cmb2' ),
		'id'   => $prefix . 'checkbox',
		'type' => 'checkbox',
	) );
	







	$prefix = '_ifts_prezzi';

	/**
	 * meta box prezzi
	 */
	$cmb_prezzi = new_cmb2_box( array(
		'id'            => 'ifts_prezzi_parchi',
		'title'         => __( 'Prezzi parco ', 'cmb2' ),
		'object_types'  => array( 'parchi', ), // Post type
		'context'       => 'advanced',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
    ) );
    
    $cmb_prezzi->add_field( array(
		'name' => esc_html__( 'Prezzo intero:', 'cmb2' ),
		'desc' => esc_html__( 'Inserire il prezzo intero del parco', 'cmb2' ),
		'id'   => $prefix . 'textmoney_intero',
		'type' => 'text_money',
		'before_field' => '€', // override '$' symbol if needed
		//'repeatable' => true,
	) );

	$cmb_prezzi->add_field( array(
		'name' => esc_html__( 'Prezzo ridotto:', 'cmb2' ),
		'desc' => esc_html__( 'Inserire il prezzo ridotto del parco', 'cmb2' ),
		'id'   => $prefix . 'textmoney_ridotto',
		'type' => 'text_money',
		'before_field' => '€', // override '$' symbol if needed
		//'repeatable' => true,
	) );
}