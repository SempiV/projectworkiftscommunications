<?php
get_header(); 

if ( ! is_active_sidebar( 'sidebar-main' ) ) {
 	$marinate_col_class = "col-lg-8 col-md-8 col-xs-12 col-sm-12 col-lg-offset-2 col-md-offset-2";
}
else {
 	$marinate_col_class = "col-lg-9 col-md-9 col-xs-12 col-sm-12";
}

?>

<div id="content" class="site-content">
	


	<div class="container">
		<div class="row">

		

			<article id="post-<?php the_ID(); ?>" <?php post_class('single-post-wrapper'); ?>>               
			<?php while ( have_posts() ) : the_post(); //main loop ?>                        
            <div class="<?php echo esc_attr($marinate_col_class); ?>">            
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
                    

                    <?php 
                    
                    get_template_part( 'template-parts/content', 'single' );  
					do_action('marinate_author_bio');
                    ?>
					


					</main><!-- #main -->
				</div><!-- #primary -->
	    	</div><!-- .col-md-8 -->
			<?php endwhile; // End of the loop. ?>                        
            <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
            <?php get_sidebar(); ?>
            </div>
        </article><!-- #post-<?php the_ID(); ?> -->                                                		        
    	</div><!-- .row -->
  </div><!-- .container -->
</div><!-- #content -->

        
<?php  get_footer(); ?>